/**
 * Created on 2018年3月21日 下午2:27:19
 */
package com.hkb.springboot.web;

import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import org.springframework.web.bind.annotation.RestControllerAdvice;
import springfox.documentation.annotations.ApiIgnore;

/**
 * 日志controller测试 . <br>
 * 
 * @author hkb <br>
 */
@RestController
@Api(tags = { "日志接口" })
public class LogController {

    /**
     * 日志对象
     */
    private final Logger LOG = LoggerFactory.getLogger(this.getClass());

    @GetMapping("/log")
    public String getlogs() {
        LOG.debug("日志测试 debug");
        LOG.info("日志测试 info");
        LOG.warn("日志测试 warn");
        LOG.error("日志测试 error");
        return "success";
    }

    @GetMapping("/error")
    public int error(){
        int i = 9/0;
        return i;
    }
}
