/**
 * Created on 2018年3月23日 下午2:56:40
 */
package com.hkb.springboot.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * Swagger2配置类 . <br>
 * 访问路径: http://localhost:8080/springboot/swagger-ui.html
 * 
 * @author hkb <br>
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {

    @Bean
    public Docket createRestApi() {
        return new Docket(DocumentationType.SWAGGER_2).apiInfo(apiInfo()).select()
                .apis(RequestHandlerSelectors.basePackage("com.hkb.springboot.web")).paths(PathSelectors.any()).build();
    }

    private ApiInfo apiInfo() {
        String title = "悦米科技 达能网盘--知识星球";
        String version = "1.0.0";
        return new ApiInfoBuilder().title(title).version(version).description("API 请遵循Rest API风格!").build();
    }

}
