/**
 * Created on 2018年3月11日 下午9:03:56
 */
package com.hkb.springboot;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.core.env.Environment;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * SpringBoot启动类 . <br>
 * 继承SpringBootServletInitializer类并重写configure方法 <br>
 * 打包成war包的形式 <br>
 * ServletComponentScan表示开启servlet的注解 <br>
 *
 * @author hkb <br>
 */
@SpringBootApplication
@ServletComponentScan
public class StartApplication extends SpringBootServletInitializer {

    private static final Logger logger = LoggerFactory.getLogger(StartApplication.class);

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(StartApplication.class);
    }

    /**
     * 隐藏banner启动方式
     */
    public static void main(String[] args) throws UnknownHostException {
        /*SpringApplication springApplication = new SpringApplication(StartApplication.class);
        // 设置banner的模式为隐藏
        springApplication.setBannerMode(Banner.Mode.CONSOLE);
        // 启动springboot应用程序
        springApplication.run(args);*/

        // 原启动方式
        // SpringApplication.run(StartApplication.class, args);

        Environment env = SpringApplication.run(StartApplication.class, args).getEnvironment();
        String port = env.getProperty("server.port" , "8080");

        logger.info("Access URLs:\n----------------------------------------------------------\n\t"
                        + "Local: \t\thttp://127.0.0.1:{}\n\t"
                        + "External: \thttp://{}:{}\n\t" ,
                port, InetAddress.getLocalHost().getHostAddress(), port);
    }

}
