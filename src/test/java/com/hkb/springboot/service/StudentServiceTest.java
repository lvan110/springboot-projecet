/**
 * Created on 2018年3月20日 上午10:48:04
 */
package com.hkb.springboot.service;

import java.util.List;

import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;

import com.google.common.collect.Lists;
import com.hkb.springboot.dao.StudentDao;
import com.hkb.springboot.entity.StudentEntity;

/**
 * 学生信息接口测试类 . <br>
 * 
 * @author hkb <br>
 */
public class StudentServiceTest extends BaseTest {

    /**
     * 学生信息接口-被测类
     */
    @Autowired
    private StudentService studentService;

    /**
     * 学生信息dao-模拟对象
     */
    @MockBean
    private StudentDao studentDao;

    /**
     * 学生实体
     */
    private StudentEntity student;

    /**
     * 学生id
     */
    private Integer id = -99;

    /**
     * 学生姓名
     */
    private String studentName = "test";

    /**
     * 学生年龄
     */
    private Integer studentAge = 20;

    /**
     * 所有测试方法执行之前执行该方法
     */
    @Before
    public void before() {
        student = new StudentEntity(id, studentName, studentAge);
        // 设置模拟对象的返回预期值
        Mockito.when(studentDao.save(student)).thenReturn(student);
        Mockito.when(studentDao.findOne(id)).thenReturn(student);
        Mockito.when(studentDao.findStudentByAge(studentAge)).thenReturn(Lists.newArrayList(student));
        Mockito.when(studentDao.getStudentByAge(studentAge)).thenReturn(Lists.newArrayList(student));
        Mockito.when(studentDao.findAll()).thenReturn(Lists.newArrayList(student));
    }

    /**
     * 测试添加
     */
    @Test
    public void addStudentTest() {
        // 执行测试
        int studentId = studentService.addStudent(student);
        // 验证
        Assert.assertThat(studentId, Matchers.is(id));
        // 得到一个抓取器
        ArgumentCaptor<StudentEntity> personCaptor = ArgumentCaptor.forClass(StudentEntity.class);
        // 验证模拟对象的save()是否被调用一次,并抓取调用时传入的参数值
        Mockito.verify(studentDao).save(personCaptor.capture());
        // 获取抓取到的参数值
        StudentEntity addStudent = personCaptor.getValue();
        // 验证调用时的参数值
        Assert.assertThat(studentName, Matchers.is(addStudent.getName()));
    }

    /**
     * 测试修改
     */
    @Test
    public void updateStudentTest() {
        // 执行测试
        int studentId = studentService.updateStudent(student);
        // 验证
        Assert.assertThat(studentId, Matchers.is(id));
        // 得到一个抓取器
        ArgumentCaptor<StudentEntity> personCaptor = ArgumentCaptor.forClass(StudentEntity.class);
        // 验证模拟对象的save()是否被调用一次,并抓取调用时传入的参数值
        Mockito.verify(studentDao).save(personCaptor.capture());
        // 获取抓取到的参数值
        StudentEntity addStudent = personCaptor.getValue();
        // 验证调用时的参数值
        Assert.assertThat(studentName, Matchers.is(addStudent.getName()));
    }

    /**
     * 测试删除
     */
    @Test
    public void deleteStudentByIdTest() {
        // 执行测试
        boolean result = studentService.deleteStudentById(id);
        // 验证模拟对象的delete()是否被调用一次
        Mockito.verify(studentDao).delete(id);
        // 验证
        Assert.assertThat(result, Matchers.is(true));
    }

    /**
     * 测试根据id查询
     */
    @Test
    public void findStudentByIdTest() {
        // 执行测试
        StudentEntity student = studentService.findStudentById(id);
        // 验证查询是否成功
        Assert.assertThat(studentName, Matchers.is(student.getName()));
    }

    /**
     * 测试根据年龄查询
     */
    @Test
    public void findStudentByAgeTest() {
        List<StudentEntity> students = studentService.findStudentByAge(studentAge);
        // 验证查询是否成功
        Assert.assertThat(1, Matchers.is(students.size()));
        Assert.assertThat(studentName, Matchers.is(student.getName()));
    }

    /**
     * 测试根据年龄查询
     */
    @Test
    public void getStudentByAgeTest() {
        List<StudentEntity> students = studentService.getStudentByAge(studentAge);
        // 验证查询是否成功
        Assert.assertThat(1, Matchers.is(students.size()));
        Assert.assertThat(studentName, Matchers.is(student.getName()));
    }

    /**
     * 测试查询所有
     */
    @Test
    public void findAllStudentTest() {
        // 执行测试
        List<StudentEntity> students = studentService.findAllStudent();
        // 验证查询是否成功
        Assert.assertThat(1, Matchers.is(students.size()));
        Assert.assertThat(studentName, Matchers.is(student.getName()));
    }

}
