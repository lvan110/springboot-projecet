/**
 * Created on 2018年3月21日 下午6:02:53
 */
package com.hkb.springboot.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

/**
 * 拦截器(登录拦截)-在LoginConfig注册 . <br>
 * 
 * @author hkb <br>
 */
public class LoginInterceptor implements HandlerInterceptor {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
            throws Exception {
        // controller方法调用之前
        // String url = request.getRequestURI();
        // TODO 登录不做拦截
        // 验证session是否存在
        // Object object = request.getSession().getAttribute("sessionUser");
        // if (object == null) {
        // // 跳转到首页
        // response.sendRedirect(request.getContextPath() + "/html/index.html");
        // return false;
        // }
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
            ModelAndView modelAndView) throws Exception {
        // 请求处理之后进行调用,但是在视图被渲染之前,即Controller方法调用之后
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
            throws Exception {

    }

}
