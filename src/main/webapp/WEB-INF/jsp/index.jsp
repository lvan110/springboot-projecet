<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<html>
<head>
<title>文件上传demo</title>
</head>
<body>
	单个文件上传->
	<br />
	<form action="/springboot/upload" method="post"
		enctype="multipart/form-data">
		<input type="file" name="file" />
		<br /> <br />
		<input type="submit" value="单个文件上传" />
	</form>
	<br /> 多个文件上传->
	<form action="/springboot/uploads" method="post"
		enctype="multipart/form-data">
		文件1:
		<input type="file" name="files" />
		<br /> 文件2:
		<input type="file" name="files" />
		<br /> 文件3:
		<input type="file" name="files" />
		<br /> <br />
		<input type="submit" value="多个文件上传" />
	</form>
</body>
</html>