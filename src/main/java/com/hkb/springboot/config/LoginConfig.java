/**
 * Created on 2018年3月30日 上午9:33:46
 */
package com.hkb.springboot.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.hkb.springboot.interceptor.LoginInterceptor;

/**
 * 登录配置 . <br>
 * 
 * @author hkb <br>
 */
@Configuration
public class LoginConfig extends WebMvcConfigurerAdapter {

    /**
     * 添加拦截器
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new LoginInterceptor()).addPathPatterns("/**");
    }

}
